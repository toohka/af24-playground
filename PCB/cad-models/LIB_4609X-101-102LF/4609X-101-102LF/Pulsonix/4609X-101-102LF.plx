PULSONIX_LIBRARY_ASCII "SamacSys ECAD Model"
//12902637/347625/2.50/9/0/Resistor Network

(asciiHeader
	(fileUnits MM)
)
(library Library_1
	(padStyleDef "c136.5_h75.8"
		(holeDiam 0.758)
		(padShape (layerNumRef 1) (padShapeType Ellipse)  (shapeWidth 1.365) (shapeHeight 1.365))
		(padShape (layerNumRef 16) (padShapeType Ellipse)  (shapeWidth 1.365) (shapeHeight 1.365))
	)
	(textStyleDef "Normal"
		(font
			(fontType Stroke)
			(fontFace "Helvetica")
			(fontHeight 1.27)
			(strokeWidth 0.127)
		)
	)
	(patternDef "4609X101102LF" (originalName "4609X101102LF")
		(multiLayer
			(pad (padNum 1) (padStyleRef c136.5_h75.8) (pt 0.000, 0.000) (rotation 90))
			(pad (padNum 2) (padStyleRef c136.5_h75.8) (pt 2.540, 0.000) (rotation 90))
			(pad (padNum 3) (padStyleRef c136.5_h75.8) (pt 5.080, 0.000) (rotation 90))
			(pad (padNum 4) (padStyleRef c136.5_h75.8) (pt 7.620, 0.000) (rotation 90))
			(pad (padNum 5) (padStyleRef c136.5_h75.8) (pt 10.160, 0.000) (rotation 90))
			(pad (padNum 6) (padStyleRef c136.5_h75.8) (pt 12.700, 0.000) (rotation 90))
			(pad (padNum 7) (padStyleRef c136.5_h75.8) (pt 15.240, 0.000) (rotation 90))
			(pad (padNum 8) (padStyleRef c136.5_h75.8) (pt 17.780, 0.000) (rotation 90))
			(pad (padNum 9) (padStyleRef c136.5_h75.8) (pt 20.320, 0.000) (rotation 90))
		)
		(layerContents (layerNumRef 18)
			(attr "RefDes" "RefDes" (pt 9.783, 0.000) (textStyleRef "Normal") (isVisible True))
		)
		(layerContents (layerNumRef 28)
			(line (pt -1.245 1.245) (pt 21.565 1.245) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt 21.565 1.245) (pt 21.565 -1.245) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt 21.565 -1.245) (pt -1.245 -1.245) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt -1.245 -1.245) (pt -1.245 1.245) (width 0.025))
		)
		(layerContents (layerNumRef 18)
			(line (pt -1.245 1.245) (pt 21.565 1.245) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt 21.565 1.245) (pt 21.565 -1.245) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt 21.565 -1.245) (pt -1.245 -1.245) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -1.245 -1.245) (pt -1.245 1.245) (width 0.1))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt -3 2.245) (pt 22.565 2.245) (width 0.1))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt 22.565 2.245) (pt 22.565 -2.245) (width 0.1))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt 22.565 -2.245) (pt -3 -2.245) (width 0.1))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt -3 -2.245) (pt -3 2.245) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -2 0) (pt -2 0) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(arc (pt -1.9, 0) (radius 0.1) (startAngle 180) (sweepAngle 180.0) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt -1.8 0) (pt -1.8 0) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(arc (pt -1.9, 0) (radius 0.1) (startAngle .0) (sweepAngle 180.0) (width 0.2))
		)
	)
	(symbolDef "4609X-101-102LF" (originalName "4609X-101-102LF")

		(pin (pinNum 1) (pt 0 mils 0 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -25 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 2) (pt 0 mils -100 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -125 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 3) (pt 0 mils -200 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -225 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 4) (pt 0 mils -300 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -325 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 5) (pt 0 mils -400 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -425 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 6) (pt 0 mils -500 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -525 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 7) (pt 0 mils -600 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -625 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 8) (pt 0 mils -700 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -725 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 9) (pt 0 mils -800 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -825 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(line (pt 200 mils 100 mils) (pt 600 mils 100 mils) (width 6 mils))
		(line (pt 600 mils 100 mils) (pt 600 mils -900 mils) (width 6 mils))
		(line (pt 600 mils -900 mils) (pt 200 mils -900 mils) (width 6 mils))
		(line (pt 200 mils -900 mils) (pt 200 mils 100 mils) (width 6 mils))
		(attr "RefDes" "RefDes" (pt 650 mils 300 mils) (justify Left) (isVisible True) (textStyleRef "Normal"))
		(attr "Type" "Type" (pt 650 mils 200 mils) (justify Left) (isVisible True) (textStyleRef "Normal"))

	)
	(compDef "4609X-101-102LF" (originalName "4609X-101-102LF") (compHeader (numPins 9) (numParts 1) (refDesPrefix RN)
		)
		(compPin "1" (pinName "1") (partNum 1) (symPinNum 1) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "2" (pinName "2") (partNum 1) (symPinNum 2) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "3" (pinName "3") (partNum 1) (symPinNum 3) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "4" (pinName "4") (partNum 1) (symPinNum 4) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "5" (pinName "5") (partNum 1) (symPinNum 5) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "6" (pinName "6") (partNum 1) (symPinNum 6) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "7" (pinName "7") (partNum 1) (symPinNum 7) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "8" (pinName "8") (partNum 1) (symPinNum 8) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "9" (pinName "9") (partNum 1) (symPinNum 9) (gateEq 0) (pinEq 0) (pinType Unknown))
		(attachedSymbol (partNum 1) (altType Normal) (symbolName "4609X-101-102LF"))
		(attachedPattern (patternNum 1) (patternName "4609X101102LF")
			(numPads 9)
			(padPinMap
				(padNum 1) (compPinRef "1")
				(padNum 2) (compPinRef "2")
				(padNum 3) (compPinRef "3")
				(padNum 4) (compPinRef "4")
				(padNum 5) (compPinRef "5")
				(padNum 6) (compPinRef "6")
				(padNum 7) (compPinRef "7")
				(padNum 8) (compPinRef "8")
				(padNum 9) (compPinRef "9")
			)
		)
		(attr "TME Electronic Components Part Number" "")
		(attr "TME Electronic Components Price/Stock" "")
		(attr "Manufacturer_Name" "Bourns")
		(attr "Manufacturer_Part_Number" "4609X-101-102LF")
		(attr "Description" "Resistor Networks & Arrays 9pins 1Kohms Bussed")
		(attr "<Hyperlink>" "")
		(attr "<Component Height>" "5.08")
		(attr "<STEP Filename>" "4609X-101-102LF.stp")
		(attr "<STEP Offsets>" "X=0;Y=0;Z=0")
		(attr "<STEP Rotation>" "X=0;Y=0;Z=0")
	)

)
