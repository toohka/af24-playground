*PADS-LIBRARY-PART-TYPES-V9*

SN74HC02N DIP794W53P254L1930H508Q14N I ANA 7 1 0 0 0
TIMESTAMP 2024.03.16.20.05.28
"TME Electronic Components Part Number" 
"TME Electronic Components Price/Stock" 
"Manufacturer_Name" Texas Instruments
"Manufacturer_Part_Number" SN74HC02N
"Description" Quad 2i/p NOR gate,SN74HC02N DIP14 25pcs
"Datasheet Link" https://datasheet.datasheetarchive.com/originals/distributors/SFDatasheet-6/sf-000133830.pdf
"Geometry.Height" 5.08mm
GATE 1 14 0
SN74HC02N
1 0 U 1Y
2 0 U 1A
3 0 U 1B
4 0 U 2Y
5 0 U 2A
6 0 U 2B
7 0 U GND
14 0 U VCC
13 0 U 4Y
12 0 U 4B
11 0 U 4A
10 0 U 3Y
9 0 U 3B
8 0 U 3A

*END*
*REMARK* SamacSys ECAD Model
181919/347625/2.50/14/3/Integrated Circuit
