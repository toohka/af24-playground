# Elektronický retrokoutek pro Animefest 2024
## Electronic playground for Animefest 2024 (english version follows)
Repozitář obsahuje schémata a návrhy všech desek tištěných spojů, které byly v retrokoutku použity, soubory pro jejich výrobu, datasheety součástek a jejich cadové modely, pokud byly vyžadovány, zdrojové kódy programů pro mikrokontrolery a veškerou dokumentaci.

## Stavebnice logických hradel
Exponát si bere za cíl zábavnou formou přiblížit princip fungování druhé nejnižší úrovně všech dnešních procesorů - CMOS hradel. Na samostatných deskách obsahuje tato hradla:

- NAND ([SN74HC00N](https://www.tme.eu/cz/details/sn74hc00n/hradla-invertory/texas-instruments/))
- NOR ([SN74HC02N](https://www.tme.eu/cz/details/sn74hc02n/hradla-invertory/texas-instruments/))
- NOT ([SN74HC05N](https://www.tme.eu/cz/details/sn74hc05n/hradla-invertory/texas-instruments/))
- AND ([SN74HC08N](https://www.tme.eu/cz/details/sn74hc08n/hradla-invertory/texas-instruments/))
- OR ([SN74HC32N](https://www.tme.eu/cz/details/sn74hc32n/hradla-invertory/texas-instruments/))
- XOR ([SN74HC86N](https://www.gme.cz/v/1490754/texas-instruments-sn74hc86n-cmos-4x-2vstupove-hradlo-xor))

Každé hradlo (vyjma invertoru) má připojené 2 vstupy a jeden výstup. Logickou hodnotu na jednotlivých vstupech lze měnit buď manuálně pomocí přepínače, nebo připojením výstupu dalšího hradla na vstup.
Na výstup je pak připojena LED indikující logickou hodnotu výstupu.

Kromě propojení výstupu se vstupem je třeba propojit rovněž zemnící kontakty kvůli zajištění společného referenčního napětí celému obvodu. 

## Bludiště