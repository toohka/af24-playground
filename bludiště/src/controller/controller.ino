#include <SparkFun_TB6612.h>

//I/O
#define LED_P 13
#define LED_COUNT 36

#define POT_L A6
#define POT_R A7

#define SWU_L 11
#define SWD_L 12
#define SWU_R 9
#define SWD_R 10

#define BOARD_TYPECHK_L 16
#define BOARD_TYPECHK_R 17

#define RELAY_L 14
#define RELAY_R 15

#define TRESHOLD_BRAKE_L 18
#define TRESHOLD_BRAKE_R 19

//MOTOR DRIVER
#define PWM_R 3
#define IN1_R 2
#define IN2_R 4
#define PWM_L 6
#define IN1_L 5
#define IN2_L 7
#define STBY 8

constexpr int offset_L = 1;
constexpr int offset_R = 1;

Motor motor_L = Motor(IN1_L, IN2_L, PWM_L, offset_L, STBY);
Motor motor_R = Motor(IN1_R, IN2_R, PWM_R, offset_R, STBY);

void setup() {
    pinMode(LED_P, OUTPUT);

    pinMode(POT_L, INPUT);
    pinMode(POT_R, INPUT);

    pinMode(SWU_L, INPUT_PULLUP);
    pinMode(SWD_L, INPUT_PULLUP);
    pinMode(SWU_R, INPUT_PULLUP);
    pinMode(SWD_R, INPUT_PULLUP);

    pinMode(BOARD_TYPECHK_L, INPUT_PULLUP);
    pinMode(BOARD_TYPECHK_R, INPUT_PULLUP);

    pinMode(RELAY_L, OUTPUT);
    pinMode(RELAY_R, OUTPUT);
}

/*
 *
 * BOARD_TYPECHK HIGH ... tlačítka
 * BOARD_TYPECHK LOW ... potenciometr
 *
 * RELAY LOW ... tlačítka
 * RELAY HIGH ... potenciometr
 */

#define PUSH_DRIVE_DURATION 1000
#define PUSH_SPEED 60
#define POT_TRESHOLD 20

volatile int ROPE_STATE_L = 0;

void loop() {
    if (digitalRead(BOARD_TYPECHK_L) == LOW) {
        // K ovládání levého motoru je připojen ovladač s tlačítky
        digitalWrite(RELAY_L, LOW); // Přepnutí vstupu na piny tlačítek
        if (digitalRead(SWU_L) == LOW) {
            motor_L.drive(PUSH_SPEED, PUSH_DRIVE_DURATION);
            motor_L.brake();
            // TODO řešit spíš přes flag než takhle, bo to zdržuje ovládání druhého motoru
            //while (digitalRead(SWU_L) == LOW);
        }
        if (digitalRead(SWD_L) == LOW) {
            motor_L.drive(-PUSH_SPEED, PUSH_DRIVE_DURATION);
            motor_L.brake();
           // while (digitalRead(SWD_L) == LOW);
        }
    } else {
        // K ovládání levého motoru je připojen ovladač s potenciometrem
        digitalWrite(RELAY_L, HIGH); // Přepnutí vstupu na piny potenciometru
        int desiredSpeed = map(analogRead(POT_L), 0, 1023, -255, 255); // Přemapování z napětí na pinu na PWM hodnotu pro rychlost otáček
        // Zabrždění při poloze potenciometru uprostřed
        if (abs(desiredSpeed) < POT_TRESHOLD) {
            motor_L.brake();
        } else {
            motor_L.drive(desiredSpeed);
        }
    }

    if (digitalRead(BOARD_TYPECHK_R) == LOW) {
        // K ovládání pravého motoru je připojen ovladač s tlačítky
        digitalWrite(RELAY_R, LOW); // Přepnutí vstupu na piny tlačítek
        if (digitalRead(SWU_R) == LOW) {
            motor_R.drive(PUSH_SPEED, PUSH_DRIVE_DURATION);
            motor_R.brake();
            //while (digitalRead(SWU_R) == LOW);
        }
        if (digitalRead(SWD_L) == LOW) {
            motor_R.drive(-PUSH_SPEED, PUSH_DRIVE_DURATION);
            motor_R.brake();
           // while (digitalRead(SWD_R) == LOW);
        }
    } else {
        // K ovládání pravého motoru je připojen ovladač s potenciometrem
        digitalWrite(RELAY_R, HIGH); // Přepnutí vstupu na piny potenciometru
        int desiredSpeed = map(analogRead(POT_R), 0, 1023, -255, 255); // Přemapování z napětí na pinu na PWM hodnotu pro rychlost otáček
        // Zabrždění při poloze potenciometru uprostřed
        if (abs(desiredSpeed) < POT_TRESHOLD) {
            motor_R.brake();
        } else {
            motor_R.drive(desiredSpeed);
        }
    }
    // TODO kontrola přepnutí lanka
}
